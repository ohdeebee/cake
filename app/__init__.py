from flask import Flask
from flask_login import LoginManager
from config import DevConfig
import os
import logging
from logging.handlers import TimedRotatingFileHandler
from flask_uploads import UploadSet, IMAGES, configure_uploads

app = Flask(__name__)
app.config.from_object(DevConfig)

lm = LoginManager()
lm.init_app(app)

# Configure the image uploading via Flask-Uploads
images = UploadSet('images', IMAGES)
configure_uploads(app, images)

from app.models import db
# instantiate SQLAclhemy
db.init_app(app)

from app.main.views import main
app.register_blueprint(main, url_prefix='')
from app.admin.views import admin
app.register_blueprint(admin, url_prefix='/admin')