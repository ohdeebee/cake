$(document).ready(function(){

    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        paginationClickable: true,
        autoplay: 10000,
        loop: true,
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        spaceBetween: 30
    });

    var swiper = new Swiper('.handpick', {
        slidesPerView: 3,
        paginationClickable: true,
        spaceBetween: 30
    });

});

$( function() {
    $( "#slider-range" ).slider({
        range: true,
        min: 1000,
        max: 100000,
        values: [ 1000, 50000 ],
        slide: function( event, ui ) {
            $( "#amount" ).val( "₦" + ui.values[ 0 ] + " - ₦" + ui.values[ 1 ] );
        }
    });
    $( "#amount" ).val( "₦" + $( "#slider-range" ).slider( "values", 0 ) +
        " - ₦" + $( "#slider-range" ).slider( "values", 1 ) );
} );


$('.value-plus').on('click', function(){
    console.log('here');
    var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.val(), 10)+1;
    divUpd.val(newVal);
});

$('.value-minus').on('click', function(){
    console.log('here');
    var divUpd = $(this).parent().find('.value'), newVal = parseInt(divUpd.val(), 10)-1;
    if(newVal>=1) divUpd.val(newVal);
});

$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    controlNav: "thumbnails"
  });
});