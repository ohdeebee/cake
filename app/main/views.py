from flask import Blueprint, redirect, render_template, flash, url_for, session
from app.models import db, Products, Categories, States, Orders, OrderItems
from app.forms import ProductForm, BillingForm

main = Blueprint('main', __name__, static_folder='static', static_url_path='/main/static', template_folder='templates')


@main.route('/index')
@main.route('/')
def index():
    products = Products.query.filter().limit(8).all()
    return render_template('main/index.html', products=products)


@main.route('/store')
def store():
    categories = Categories.query.filter(Categories.status==0).all()

    products = Products.query.filter().all()
    return render_template('main/store.html', categories=categories, products=products)


@main.route('/product/<prod_id>', methods=['GET', 'POST'])
def product(prod_id):
    product = Products.query.get(prod_id)
    form = ProductForm()

    if product:
        # check if product is active, if not redirect to store
        if product.status != 0:
            flash('This product is currently out of stock', 'error')
            return redirect(url_for('.store'))

        if form.validate_on_submit():

            # Checks to see if the user has already started a cart.
            if 'cart' in session:

                for i, d in enumerate(session['cart']):
                    if d['id'] == product.id:
                        d1 = session['cart'].pop(i)

                # ie product is not already n cart
                session['cart'].append({'id': product.id, 'name': product.name, 'quantity': form.quantity.data,
                 'image_url': product.defaultImage.url, 'price': product.getPrice})
            else:
                # In this block, the user has not started a cart, so we start it for them and add the product.
                session['cart'] = [
                    {'id': product.id, 'name': product.name, 'quantity': form.quantity.data,
                     'image_url': product.defaultImage.url, 'price': product.getPrice}
                ]

            # update cart items & total
            total = 0
            qty = 1
            for p in session['cart']:
                total = total + (int(p['quantity']) * float(p['price']))
                qty += int(p['quantity'])

            session['quantity'] = qty
            session['total'] = total

            flash('The product has been added to your cart', 'success')
            return redirect(url_for('.cart'))

        # featured products
        featured = Products.query.filter(Products.category_id == product.category_id, Products.id != product.id,
                                         Products.status == 0).limit(4).all()

        if len(featured) < 4:
            featured = Products.query.filter(Products.id != product.id).limit(4).all()

        return render_template('main/product.html', product=product, featured=featured, form=form)
    else:
        flash('This product does not exist', 'error')
        return redirect(url_for('.store'))


@main.route('/cart')
def cart():
    if 'quantity' not in session:
        flash('There are no items in your cart', 'error')
        return redirect(url_for('.store'))

    return render_template('main/cart.html')


@main.route('/remove/<prod_id>', methods=['GET'])
def remove(prod_id):
    print prod_id
    # Checks to see if the user has already started a cart.
    if 'cart' in session:
        print 'here'
        for i, d in enumerate(session['cart']):
            print d['id']
            print prod_id
            if d['id'] == int(prod_id):
                print 'h3'
                d1=session['cart'].pop(i)

        # reclculate totals and quantity
        total = 0
        qty = 0
        for p in session['cart']:
            total = total + (int(p['quantity']) * float(p['price']))
            qty += int(p['quantity'])

        session['quantity'] = qty
        session['total'] = total

        # success message
        flash('Your cart has been successfully updated', 'success')
        # redirect to cart
        return redirect(url_for('.cart'))
    else:
        flash('There are no items in your cart', 'error')
        return redirect(url_for('.store'))


@main.route('/contact')
def contact():
    return render_template('main/contact.html')


@main.route('/billing-information', methods=['GET', 'POST'])
def billing():
    form = BillingForm()
    form.state.choices = [(a.state, a.state) for a in States.query.order_by('state')]

    #print form.country.data
    if form.validate_on_submit():
        # process
        bill = Orders()

        bill.full_name = form.full_name.data
        bill.phone = form.phone.data
        bill.email = form.email.data
        bill.address = form.address.data
        bill.additional_information = form.additional.data
        bill.city = form.city.data
        bill.state = form.state.data
        bill.country = form.country.data
        bill.payment_type = form.payment_type.data
        bill.delivery_type = form.delivery_type.data

        bill.item_count = session['quantity']
        bill.total_price = session['total']

        # generate order number
        db.session.add(bill)
        db.session.flush()

        bill.order_no = "CK"+str(bill.id).zfill(5)+bill.state[:2].upper()
        db.session.add(bill)
        db.session.commit()

        # add items
        for d in session['cart']:
            item = OrderItems()

            item.order_id = bill.id
            item.product_id = d['id']
            item.product_name = d['name']
            item.quantity = d['quantity']
            item.unit_price = d['price']

            db.session.add(item)
            db.session.commit()

        # clear session
        session.clear()
        return redirect(url_for('.store'))
    return render_template('main/payment.html', form=form)


@main.route('/about')
def about():
    return render_template('main/about.html')


@main.route('/services')
def services():
    return render_template('main/services.html')