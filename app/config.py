class Config(object):
    ''' Base configuration class
    '''
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = 'mysql://root:@localhost/cake'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECRET_KEY = 'ncdue2413dgyguyyet6422376bde12345'
    UPLOAD_FOLDER = 'uploads'
    ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

    # Uploads
    UPLOADS_DEFAULT_DEST = 'app/static/uploads/products/'
    #UPLOADS_DEFAULT_URL = 'http://localhost:5000/static/uploads/products'

    UPLOADED_IMAGES_DEST = 'app/static/uploads/products/'
    UPLOADED_IMAGES_URL = 'http://localhost:5000/static/uploads/products/'


class DevConfig(Config):
    DEBUG = True


class ProductionConfig(Config):
    """
    Production config object

    """

    DEBUG = False