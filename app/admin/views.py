from flask import Blueprint, redirect, render_template, flash, url_for, request, abort, make_response, json
from werkzeug.utils import secure_filename
from flask_login import login_user, login_required, current_user, logout_user
from urlparse import urlparse, urljoin
from app import app, lm, images
from app.forms import LoginForm, CreateUserForm, CreateCategoryForm, CreateProductForm, \
    AddDiscountForm, EditProductForm, AddImageForm, DiscountForm
from app.models import db, User, Audit, Categories, Products, Pictures, DiscountHistory, Discounts, \
    list_audit_models, list_audit_types
import os

admin = Blueprint('admin', __name__, static_folder='static', template_folder='templates')
lm.login_view='.login'

@lm.user_loader
def load_user(user_id):
    return User.query.get(user_id)


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


def log_audit(user_id, desc, object_type, model, affected_id):
    audit = Audit(user_id, desc, object_type, model, affected_id)
    db.session.add(audit)
    db.session.commit()


def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']


@admin.route('/', methods=['GET', 'POST'])
@login_required
def home():
    #flash('Invalid login credentials', 'error')
    return render_template('admin/layout.html')


@admin.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    print('got here1')
    if form.validate_on_submit():
        # check if username exists
        user = User.query.filter(db.or_(User.username==str(form.username.data).lower())).one_or_none()
        print('got here2')
        if not user:
            flash('Invalid login credentials', 'error')
        else:
            # check if password is correct
            if user.check_password(form.password.data):
                login_user(user)

                # redirect to page he was going to
                next = request.args.get('next')
                # is_safe_url should check if the url is safe for redirects.
                # See http://flask.pocoo.org/snippets/62/ for an example.
                if not is_safe_url(next):
                    return abort(400)

                # log audit
                log_audit(current_user.id, 'User logged in', 0, list_audit_models[0], current_user.id)
                return redirect(next or url_for('.home'))
            else:
                flash('Invalid login credentials', 'error')
    return render_template('admin/login.html', form=form)


@admin.route('/manage-categories', methods=['GET', 'POST'])
@login_required
def manage_categories():
    form = CreateCategoryForm()

    if form.validate_on_submit():
        # process
        cat = Categories.query.filter(Categories.name==str(form.name.data)).one_or_none()

        if cat:
            flash('This category already exists', 'error')
        else:
            # save category
            cat = Categories()
            cat.name = form.name.data
            cat.description = form.description.data
            cat.created_by = current_user.id

            if 'image' in request.files:
                image = request.files['image']
                if image.filename != '':
                    # process image
                    if image and allowed_file(image.filename):
                        filename = secure_filename(image.filename)
                        image.save(os.path.join(app.static_folder, app.config['UPLOAD_FOLDER'], 'categories', filename))
                        cat.image = filename

            db.session.add(cat)
            db.session.commit()
            # log audit
            log_audit(current_user.id, 'User created a category', 2, list_audit_models[1], cat.id)
            flash('The category has been successfully created', 'success')
    # fetch all categories
    cats = Categories.query.all()

    return render_template('admin/manage_categories.html', cats=cats, form=form)


@admin.route('/toggle-category-status/<int:id>/<int:action>', methods=['GET'])
@login_required
def toggle_category_status(id, action):
    # check if action is valid
    if int(action) not in [0, 1]:
        flash('Invalid action', 'error')
        return redirect(url_for('.manage_categories'))

    # get category
    cat = Categories.query.get(id)

    if cat:
        # update status
        cat.status = action
        db.session.add(cat)
        db.session.commit()

        # set flash message
        if action == 0:
            flash('The category has been successfully activated', 'success')
            # log audit
            log_audit(current_user.id, 'User activated a category', 3, list_audit_models[1], cat.id)
        else:
            flash('The category has been suspended', 'success')
            # log audit
            log_audit(current_user.id, 'User deactivated a category', 3, list_audit_models[1], cat.id)
    else:
        flash('This category does not exist', 'error')
    # redirect
    return redirect(url_for('.manage_categories'))


@admin.route('/manage-products', methods=['GET', 'POST'])
@login_required
def manage_products():
    form = CreateProductForm()
    form.category.choices = [(a.id, a.name) for a in Categories.query.order_by(Categories.id.asc()).all()]

    if form.validate_on_submit():
        product = Products()
        product.name = form.name.data
        product.description = form.description.data
        product.category_id = form.category.data
        product.price = form.price.data
        product.created_by = current_user.id

        db.session.add(product)
        db.session.commit()

        for i in request.files:
            if request.files[i]:
                print(i)
                filename = images.save(request.files[i], folder=str(product.id), name=i+'.')
                picture = Pictures()
                picture.name = filename
                picture.product_id = product.id
                picture.url = images.url(filename)
                db.session.add(picture)
                db.session.commit()

        # log audit
        log_audit(current_user.id, 'User created a product', 6, list_audit_models[3], product.id)
        flash('The product has been successfully created', 'success')

    products = Products.query.all()
    return render_template('admin/manage_products.html', products=products, form=form)


@admin.route("/products/<int:category_id>/", methods=["GET"])
def get_product_request(category_id):
    products = Products.query.filter(Products.category_id==category_id).all()

    data = [
        (sub.id, sub.name) for sub in products
    ]
    response = make_response(json.dumps(data))
    response.content_type = 'application/json'
    return response


@admin.route('/toggle-product-status/<int:id>/<int:action>', methods=['GET'])
@login_required
def toggle_product_status(id, action):
    # check if action is valid
    if int(action) not in [0, 1]:
        flash('Invalid action', 'error')
        return redirect(url_for('.manage_products'))

    # get category
    pro = Products.query.get(id)

    if pro:
        # update status
        pro.status = action
        db.session.add(pro)
        db.session.commit()

        # set flash message
        if action == 0:
            flash('The product has been successfully activated', 'success')
            # log audit
            log_audit(current_user.id, 'User activated a product', 7, list_audit_models[3], pro.id)
        else:
            flash('The product has been deactivated', 'success')
            # log audit
            log_audit(current_user.id, 'User deactivated a product', 7, list_audit_models[3], pro.id)
    else:
        flash('This product does not exist', 'error')
    # redirect
    return redirect(url_for('.manage_products'))


@admin.route('/view-product/<int:id>', methods=['GET', 'POST'])
@login_required
def view_product(id):
    product = Products.query.get(id)

    if product:
        form = AddDiscountForm()
        imgForm = AddImageForm()

        if imgForm.upload.data:
            if imgForm.validate_on_submit():
                # process image
                if request.files['image']:
                    picture = Pictures()
                    db.session.add(picture)
                    db.session.flush()
                    filename = images.save(request.files['image'], folder=str(product.id), name=str(picture.id)+'.')

                    picture.name = filename
                    picture.product_id = product.id
                    picture.url = images.url(filename)
                    db.session.add(picture)
                    db.session.commit()

                    flash('The product image has been uploaded', 'success')
        if form.submit.data:
            if form.validate_on_submit():
                # process
                dis = Discounts.query.filter(Discounts.product_id==product.id).one_or_none()

                if dis:
                    dis.discount_price = form.price.data
                    dis.updated_by = current_user.id
                    dis.date_updated = db.func.now()
                    dis.status = 0
                else:
                    dis = Discounts()
                    dis.product_id = product.id
                    dis.discount_price = form.price.data
                    dis.created_by = current_user.id
                    dis.updated_by = current_user.id
                    dis.date_updated = db.func.now()
                    dis.status = 0
                db.session.add(dis)
                db.session.commit()
                # log audit
                log_audit(current_user.id, 'User created a discount', 7, list_audit_models[3], product.id)

                # add to discount history
                history = DiscountHistory()
                history.product_id = product.id
                history.created_by = current_user.id
                history.discount_price = form.price.data
                history.product_price = product.price

                db.session.add(history)
                db.session.commit()

                flash('The product discount has been created', 'success')
        return render_template('admin/view_product.html', product=product, form=form, imgForm=imgForm)
    else:
        flash('The product does not exist', 'error')
        return redirect(url_for('.manage_products'))


@admin.route('/delete-picture/<int:id>', methods=['GET'])
@login_required
def delete_picture(id):
    picture = Pictures.query.get(id)

    if picture:
        # remove picture
        try:
            os.remove(images.path(filename=picture.name))
        except Exception as error:
            app.logger.error("Error removing file:", error)
        # save product id
        product_id = picture.product_id
        # delete record
        db.session.delete(picture)
        db.session.commit()
        # redirect to product page
        flash('This picture has been deleted', 'success')
        return redirect(url_for('.view_product', id=product_id))
    else:
        flash('This picture does not exist', 'error')
        return redirect(url_for('.manage_products'))


@admin.route('/update-product/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_product(id):
    product = Products.query.get(id)

    if product:
        form = EditProductForm(obj=product)
        # get product categories
        form.category.choices = [(a.id, a.name) for a in Categories.query.all()]

        # process form
        if form.validate_on_submit():
            product.name = form.name.data
            product.category_id = form.category.data
            product.price = form.price.data
            product.description = form.description.data
            db.session.add(product)
            db.session.commit()

            # add audit
            log_audit(current_user.id, 'User modified a product', 7, list_audit_models[3], product.id)

            flash('The product has been successfully updated', 'success')
            return redirect(url_for('.view_product', id=product.id))

        # set defaults
        form.category.data = product.category_id

        return render_template('admin/edit_product.html', product=product, form=form)
    else:
        flash('The product does not exist', 'error')
        return redirect(url_for('.manage_products'))


@admin.route('/manage-discounts', methods=['GET', 'POST'])
@login_required
def manage_discounts():
    form = DiscountForm()
    # get product categories
    form.category.choices = [(a.id, a.name) for a in Categories.query.order_by(Categories.id.asc()).all()]
    # get first cat id
    casc = Categories.query.order_by(Categories.id.asc()).limit(1).one_or_none()

    # get products
    if casc:
        form.product.choices = [(a.id, a.name) for a in Products.query.filter(Products.category_id == casc.id).all()]

    if form.validate_on_submit():
        # process
        dis = Discounts.query.filter(Discounts.product_id == form.product.data).one_or_none()

        if dis:
            dis.discount_price = form.price.data
            dis.updated_by = current_user.id
            dis.date_updated = db.func.now()
            dis.status = 0
        else:
            dis = Discounts()
            dis.product_id = form.product.data
            dis.discount_price = form.price.data
            dis.created_by = current_user.id
            dis.updated_by = current_user.id
            dis.date_updated = db.func.now()
            dis.status = 0
        db.session.add(dis)
        db.session.commit()
        # log audit
        log_audit(current_user.id, 'User created a discount', 7, list_audit_models[3], dis.product_id)

        # add to discount history
        history = DiscountHistory()
        history.product_id = dis.product_id
        history.created_by = current_user.id
        history.discount_price = form.price.data
        history.product_price = dis.product.price

        db.session.add(history)
        db.session.commit()

        flash('The product discount has been created', 'success')

    discounts = Discounts.query.all()

    return render_template('admin/manage_discounts.html', discounts=discounts, form=form)


@admin.route('/toggle-discount-status/<int:id>/<int:action>', methods=['GET'])
@login_required
def toggle_discount_status(id, action):
    # check if action is valid
    if int(action) not in [0, 1]:
        flash('Invalid action', 'error')
        return redirect(url_for('.manage_discounts'))

    # get category
    dis = Discounts.query.get(id)

    if dis:
        # update status
        dis.status = action
        db.session.add(dis)
        db.session.commit()

        # set flash message
        if action == 0:
            flash('The discount has been successfully activated', 'success')
            # log audit
            log_audit(current_user.id, 'User reactivated product discount', 7, list_audit_models[3], dis.product_id)
        else:
            flash('The discount has been suspended', 'success')
            # log audit
            log_audit(current_user.id, 'User deactivated product discount', 7, list_audit_models[3], dis.product_id)
    else:
        flash('This category does not exist', 'error')
    # redirect
    return redirect(url_for('.manage_discounts'))


@admin.route('/view-discount-history', methods=['GET'])
@login_required
def view_discount_history():
    discounts = DiscountHistory.query.order_by(DiscountHistory.id.desc()).all()
    return render_template('admin/discount_history.html', discounts=discounts)


@admin.route('/view-audit-trail', methods=['GET'])
@login_required
def view_audit():
    audits = Audit.query.order_by(Audit.id.desc()).all()
    return render_template('admin/audit_history.html', audits=audits)


@admin.route('/manage-users', methods=['GET', 'POST'])
@login_required
def manage_users():
    form = CreateUserForm()

    if form.validate_on_submit():
        # check if user already exists
        user = User.query.filter(db.or_(User.username==str(form.username.data).lower())).one_or_none()

        if user:
            flash('This username already exists', 'error')
        else:
            # check if email already exists
            user = User.query.filter(db.or_(User.email==str(form.email.data).lower())).one_or_none()

            if user:
                flash('This email address already exists', 'error')
            else:
                # create user
                user = User(form.full_name.data, form.username.data, form.email.data, form.password.data, form.user_type.data)
                db.session.add(user)
                db.session.commit()
                flash('The user has been successfully created', 'success')
                # log audit
                log_audit(current_user.id, 'User created a new user', 1, list_audit_models[0], user.id)

    # fetch all users
    users = User.query.all()

    return render_template('admin/manage_users.html', users=users, form=form)


@admin.route('/toggle-user-status/<int:id>/<int:action>', methods=['GET'])
@login_required
def toggle_user_status(id, action):
    if current_user.id == id:
        flash('Operation not allowed. You cannot toggle your own status', 'error')
        return redirect(url_for('.manage_users'))
    # check if action is valid
    if int(action) not in [0, 1]:
        flash('Invalid action', 'error')
        return redirect(url_for('.manage_users'))

    # get user
    user = User.query.get(id)

    if user:
        # update status
        user.status = action
        db.session.add(user)
        db.session.commit()

        # set flash message
        if action == 0:
            flash('The user has been successfully activated', 'success')
            # log audit
            log_audit(current_user.id, 'User reactivated a user', 1, list_audit_models[0], user.id)
        else:
            flash('The user has been deactivated', 'success')
            # log audit
            log_audit(current_user.id, 'User deactivated a user', 1, list_audit_models[0], user.id)
    else:
        flash('This user does not exist', 'error')
    # redirect
    return redirect(url_for('.manage_users'))


@admin.route('/logout', methods=['GET'])
@login_required
def logout():
    # log audit
    log_audit(current_user.id, 'User logged out', 0, list_audit_models[0], current_user.id)
    logout_user()
    return redirect(url_for('.login'))