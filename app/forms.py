from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SelectField, HiddenField, \
    DecimalField, TextAreaField, RadioField, SubmitField, SelectMultipleField, IntegerField
from wtforms.validators import DataRequired, EqualTo, Length, Email
from flask_wtf.file import FileField, FileAllowed
from . import images

class SpecialSelect(SelectField):
    '''
        When submitting an ajax populated select box, pre-validate causes error
    '''

    def pre_validate(self, form):
        pass


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])


class CreateUserForm(FlaskForm):
    full_name = StringField('Full Name', validators=[DataRequired(), Length(max=50)])
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=50)])
    email = StringField('Email', validators=[DataRequired(), Length(min=2, max=50), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', [DataRequired(),
                                                          EqualTo('password', 'The passwords do not match')])
    user_type = SelectField('Account Type', choices=[(1, 'Basic User'), (2, 'Administrator')], coerce=int)


class CreateCategoryForm(FlaskForm):
    name = StringField('Category Name', validators=[DataRequired(), Length(max=50)])
    description = TextAreaField('Description')
    image = FileField('Category Image', validators=[FileAllowed(images, 'Images only!')])


class CreateProductForm(FlaskForm):
    name = StringField('Product Name', validators=[DataRequired(), Length(max=50)])
    description = TextAreaField('Description')
    price = DecimalField('Price')
    category = SelectField('Category', coerce=int)
    image1 = FileField('Main Image', validators=[FileAllowed(images, 'Images only!')])
    image2 = FileField('Other Images', validators=[FileAllowed(images, 'Images only!')])
    image3 = FileField('Other Images', validators=[FileAllowed(images, 'Images only!')])
    image4 = FileField('Other Images', validators=[FileAllowed(images, 'Images only!')])


class AddImageForm(FlaskForm):
    image = FileField('Image', validators=[DataRequired(), FileAllowed(images, 'Images only!')])
    upload = SubmitField('Upload Image')


class EditProductForm(FlaskForm):
    name = StringField('Product Name', validators=[DataRequired(), Length(max=50)])
    description = TextAreaField('Description')
    price = DecimalField('Price')
    category = SelectField('Category', coerce=int)


class AddDiscountForm(FlaskForm):
    product_id = HiddenField('Product ID', validators=[DataRequired()])
    price = DecimalField('Discount Price', validators=[DataRequired()])
    submit = SubmitField('Submit')


class DiscountForm(FlaskForm):
    category = SelectField('Category', coerce=int)
    product = SpecialSelect('Product', choices=(), coerce=int, validators=[DataRequired()])
    price = DecimalField('Price')


class ProductForm(FlaskForm):
    quantity = StringField('Quantity')


class BillingForm(FlaskForm):
    full_name = StringField('Full Name', validators=[DataRequired(), Length(max=100)])
    phone = StringField('Phone Number', validators=[DataRequired(), Length(max=50)])
    email = StringField('Email Address', validators=[DataRequired(), Email()])
    address = TextAreaField('Address', validators=[DataRequired()])
    additional = TextAreaField('Additional')
    city = StringField('City', validators=[DataRequired(), Length(max=50)])
    state = SelectField('State')
    country = SelectField('Country', choices=[('Nigeria', 'Nigeria')])
    payment_type = StringField()
    delivery_type = StringField()
    #terms = IntegerField(validators=[DataRequired()])