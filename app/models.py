# -*- coding: utf-8 -*-
from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
from sqlalchemy import Column, Integer, String, DateTime, func, Text, Float
from werkzeug.security import generate_password_hash, check_password_hash

db = SQLAlchemy()

list_audit_types = ['User Access', 'User Management', 'Category Creation', 'Category Mangement', 'Sub-Category Creation',
                    'Sub-Category Management', 'Product Creation', 'Product Management']
list_audit_models = ['User', 'Category', 'Sub-Category', 'Products', 'Discount']


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    full_name = Column(String(100))
    username = Column(String(50), unique=True)
    email = Column(String(50), unique=True)
    password_hash = Column(String(200))
    account_type = Column(Integer, default=1)
    # 1 for Basic: 2 for Super
    status = Column(Integer, default=0)
    # 0 for Active: 1 for Inactive
    date_created = Column(DateTime, default=func.now())

    def __repr__(self):
        return self.full_name

    def __init__(self, full_name, username, email, password, account_type):
        self.full_name = full_name
        self.username = username.lower()
        self.email = email.lower()
        self.set_password(password)
        self.account_type = account_type

    @property
    def user_type(self):
        if self.account_type == 2:
            return 'Administrator'
        else:
            return 'Basic User'

    @property
    def statusText(self):
        if self.status == 0:
            return 'Active'
        return 'Suspended'

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class Categories(db.Model):
    __tablename__ = 'categories'

    id = Column(Integer, primary_key=True)
    name = Column(String(100))
    description = Column(Text)
    image = Column(String(100))
    status = Column(Integer, default=0)
    created_by = Column(Integer, db.ForeignKey('users.id'))
    # 0 for Active: 1 for Inactive
    date_created = Column(DateTime, default=func.now())

    products = db.relationship("Products")
    creator = db.relationship("User", uselist=False)

    def __repr__(self):
        return self.name

    @property
    def numberProducts(self):
        return len(self.products)

    @property
    def statusText(self):
        if self.status == 0:
            return 'Active'
        return 'Inactive'


class Products(db.Model):
    __tablename__ = 'products'

    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    description = Column(Text)
    price = Column(Float(precision=5))
    category_id = Column(Integer, db.ForeignKey('categories.id'))
    created_by = Column(Integer, db.ForeignKey('users.id'))
    date_created = Column(DateTime, default=func.now())
    status = Column(Integer, default=0)
    # 0 for Active: 1 for Inactive

    pictures = db.relationship("Pictures")
    creator = db.relationship("User", uselist=False)
    category = db.relationship("Categories", uselist=False)
    discount = db.relationship("Discounts", uselist=False)

    def __repr__(self):
        return self.name

    @property
    def pictureCount(self):
        return len(self.pictures)

    @property
    def defaultImage(self):
        return self.pictures[0]

    @property
    def nairaPrice(self):
        #return '₦'+str(self.price)
        return '{:20,.2f}'.format(self.price)

    @property
    def statusText(self):
        if self.status == 0:
            return 'Active'
        return 'Inactive'

    @property
    def discounted(self):
        if self.discount:
            if self.discount.status == 0:
                return True
        return False

    @property
    def discountedText(self):
        if self.discounted:
            return 'Yes'
        return 'No'

    @property
    def getPrice(self):
        if self.discounted:
            return self.discount.discount_price
        else:
            return self.price

    @property
    def date(self):
        return self.date_created.strftime("%d %b %Y %H:%M")


class Discounts(db.Model):
    __tablename__ = 'discounts'

    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, db.ForeignKey('products.id'))
    discount_price = Column(Float(precision=5))
    created_by = Column(Integer, db.ForeignKey('users.id'))
    date_created = Column(DateTime, default=func.now())
    updated_by = Column(Integer, db.ForeignKey('users.id'))
    date_updated = Column(DateTime)
    status = Column(Integer, default=0)

    product = db.relationship("Products", uselist=False)
    creator = db.relationship("User", uselist=False, primaryjoin="User.id == Discounts.created_by")
    last_update = db.relationship("User", uselist=False, primaryjoin="User.id == Discounts.updated_by")

    def format_price(self, value):
        return '{:20,.2f}'.format(value)

    @property
    def nairaPrice(self):
        #return '₦'+str(self.price)
        return '{:20,.2f}'.format(self.discount_price)

    def date(self, value):
        return value.strftime("%d %b %Y %H:%M")

    @property
    def statusText(self):
        if self.status == 0:
            return 'Active'
        return 'Inactive'


class DiscountHistory(db.Model):
    __tablename__ = 'discount_history'

    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, db.ForeignKey('products.id'))
    created_by = Column(Integer, db.ForeignKey('users.id'))
    product_price = Column(Float)
    discount_price = Column(Float)
    date_created = Column(DateTime, default=func.now())

    product = db.relationship("Products", uselist=False)
    creator = db.relationship("User", uselist=False)

    def date(self, value):
        return value.strftime("%d %b %Y %H:%M")

    def format_price(self, value):
        return '{:20,.2f}'.format(value)


class Pictures(db.Model):
    __tablename__ = 'pictures'

    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, db.ForeignKey('products.id'))
    name = Column(String(50))
    url = Column(String(100))
    date_created = Column(DateTime, default=func.now())

    product = db.relationship("Products", uselist=False)


class Orders(db.Model):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True)
    order_no = Column(String(50), unique=True)
    full_name =  Column(String(100))
    phone = Column(String(50))
    email = Column(String(50))
    address = Column(Text)
    city = Column(String(50))
    state = Column(String(50))
    country = Column(String(50))
    additional_information = Column(Text)
    payment_type = Column(Integer)
    delivery_type = Column(Integer)
    item_count = Column(Integer)
    total_price = Column(Float)
    status = Column(Integer, default=0)
    date_created = Column(DateTime, default=func.now())

    items = db.relationship("OrderItems")

    @property
    def payment(self):
        if self.payment_type == 1:
            return "Pay on delivery"

    @property
    def delivery(self):
        if self.delivery_type == 1:
            return "Delivery"
        else:
            return "Pickup"

class OrderItems(db.Model):
    __tablename__ = 'order_items'

    id = Column(Integer, primary_key=True)
    order_id = Column(Integer, db.ForeignKey('orders.id'))
    product_id = Column(Integer, db.ForeignKey('products.id'))
    product_name = Column(String(50))
    quantity = Column(Integer)
    unit_price = Column(Float)
    date_created = Column(DateTime, default=func.now())

    product = db.relationship("Products", uselist=False)
    order = db.relationship("Orders", uselist=False)


class States(db.Model):
    __tablename__ = 'states'

    id = Column(Integer, primary_key=True)
    state = Column(String(50))


class Audit(db.Model):
    __tablename__ = 'audit'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, db.ForeignKey('users.id'))
    description = Column(Text)
    action_type = Column(Integer)
    action_model = Column(String(100))
    item_id = Column(Integer)
    date_created = Column(DateTime, default=func.now())

    product = db.relationship("Products", foreign_keys="Audit.item_id", primaryjoin="and_(Products.id==Audit.item_id,"
                                                                                " Audit.action_model == 'Products')",
                           uselist=False)
    category = db.relationship("Categories", foreign_keys="Audit.item_id", primaryjoin="and_(Categories.id==Audit.item_id,"
                                                                          " Audit.action_model == 'Category')",
                        uselist=False)
    discount = db.relationship("Discounts", foreign_keys="Audit.item_id",
                                   primaryjoin="and_(Discounts.id==Audit.item_id, Audit.action_model == 'Discount')",
                                   uselist=False)
    user = db.relationship("User", foreign_keys="Audit.item_id", primaryjoin="and_(User.id==Audit.item_id,"
                                                                          " Audit.action_model == 'User')",
                        uselist=False)

    owner = db.relationship("User", uselist=False)

    def __init__(self, user_id, desc, action_type, action_model, item_id):
        self.user_id = user_id
        self.description = desc
        self.action_type = action_type
        self.action_model = action_model
        self.item_id = item_id

    @property
    def get_item(self):
        if self.action_model == 'User':
            return self.user
        elif self.action_model == 'Products':
            return self.product
        elif self.action_model == 'Category':
            return self.category
        elif self.action_model == 'Discount':
            return self.discount

    @property
    def category(self):
        return list_audit_types[self.action_type]

    def date(self, value):
        return value.strftime("%d %b %Y %H:%M")

